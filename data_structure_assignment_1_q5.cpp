//
//  main.cpp
//  Data_structure_assignment_1_q5
//
//

#include <iostream>
#include <math.h>
using namespace std;

int COUNT_ = 0;

long multGood (long m, long n) {
    cout << "count = " << ++COUNT_ << endl;
    if (n == 1) return m;
    if (n % 2 == 0)
        // (m * n/2 + m * n/2);
        return (multGood(m, n/2) * 2);
        
    else
        // (m * floor(n/2) + m * floor(n/2) + m);
        return (multGood(m, floor(n/2)) * 2 + m);
}

int main(int argc, const char * argv[]) {
    int m, n;
    cout << "Please enter m and n to calculate m * n: ";
    cin >> m >> n;
    cout << multGood(m, n) << "\nm * n = " << endl;
    return 0;
}


/*#include <iostream>
using namespace std;
int count = 0;
int multBad (long m, long n){
    cout << "\ncount = " << ++count; if (n == 1) return m;
    else return (m + multBad(m, n-1));
}
int main(){
    int m, n;
    cout << "Please enter m and n to calculate m * n: ";
    cin >> m >> n;
    cout << "\nm * n = " << multBad (m, n) << endl;
    //getch();
}*/
